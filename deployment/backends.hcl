job "qmgr" {
    type        = "service"
    datacenters = ["dc1"]
    group "rabbitmq-nodes" {
        count = 1
        network {
            port "management" {
                static = 15672
            }
            port "rabbitmq" {
                static = 5672
            }
        }
        task "node" {
            driver = "docker"
            config {
                image = "rabbitmq:management-alpine"
                ports = [ "management", "rabbitmq" ]
            }
            env {
                RABBITMQ_ERLANG_COOKIE = "kw5%YGzYHQjxSmFrizJ4TEARU*KX23"
                RABBITMQ_DEFAULT_USER = "admin"
                RABBITMQ_DEFAULT_PASS = "password"
                RABBITMQ_DEFAULT_VHOST = "vhost"
            }
            resources {
                cpu    = 256
                memory = 512
            }
        }
    }
    group "mongodb-nodes" {
        count = 1
        task "mongodb-node" {
            driver = "docker"
            config {
                image = "mongo:4.4.6"
                mount = {
                    type = "volume"
                    target = "/data/db"
                    source = "${NOMAD_GROUP_NAME}-${NOMAD_ALLOC_INDEX}"
                    readonly = false
                }
                ports = ["mongodb"]
            }
            resources {
                cpu    = 256
                memory = 512

            }
        }
        restart {
            # The number of attempts to run the job within the specified interval.
            attempts = 5
            interval = "5m"
            # The "delay" parameter specifies the duration to wait before restarting
            # a task after it has failed.
            delay = "25s"
            mode = "delay"
        }
        network {
            port "mongodb" {
                static = 27017
            }
        }
        ephemeral_disk {
            size = 3000
    }
    }
     group "redis-nodes" {
        count = 1
        task "redis-node" {
            driver = "docker"
            config {
                image = "redis:3.2"
                ports = ["redis"]
            }
            resources {
                cpu    = 256
                memory = 512

            }
        }
        restart {
            # The number of attempts to run the job within the specified interval.
            attempts = 5
            interval = "5m"
            # The "delay" parameter specifies the duration to wait before restarting
            # a task after it has failed.
            delay = "25s"
            mode = "delay"
        }
        network {
            port "redis" {
                static = 6379
            }
        }
    }
    group "fluentd-nodes" {
        count = 1
        task "node" {
            driver = "docker"
            config {
                image = "fluent/fluentd:edge-debian"
                volumes = [
                    "local/fluentd.conf:/fluentd/etc/fluent.conf",
                ]
                ports = ["http", "syslog", "forward"]
            }
            template {
                data = <<EOF
<source>
  @type forward
  port 24224
  bind 0.0.0.0
</source>
<filter api>
  @type parser
  key_name log
  <parse>
    @type json
    time_type string
  </parse>
</filter>
<match api>
  @type stdout
</match>
EOF

              destination = "local/fluentd.conf"
            }
    resources {
        cpu    = 512
        memory = 768
    }
    }
    network {
        port "http" {
            static = 9880
        }
        port "syslog" {
            static = 5140
        }
        port "forward" {
            static = 24224
        }
    }
    }
}