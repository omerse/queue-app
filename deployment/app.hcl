locals {
    version = 0.6
}

job "queue-app" {
    type        = "service"
    datacenters = ["dc1"]
    meta {
        VERSION = "${local.version}"
    }
    group "workers" {
        count = 3
        scaling {
            enabled = true
            min = 0
            max = 10
            policy {
            }
        }
        update {
            max_parallel     = 2
            health_check =   "task_states"
            min_healthy_time = "10s"
            healthy_deadline = "1m"
            auto_revert = true
        }
        task "worker" {
            driver = "docker"
            config {
                image = "192.168.1.5:5555/workers:${local.version}"
                args = ["celery","-A","workers.tasks","worker","--loglevel=INFO","-Q","process_scan,gen_prime,add"]
                labels {
                    group = "celery-workers"
                }
                logging {
                    type = "fluentd"
                    config {
                        fluentd-address = "192.168.1.5:24224"
                        tag = "workers"
                    }
                }
            }
            resources {
                cpu    = 256
                memory = 256
            }
        }
    }
    group "scheduelers" {
        count = 1
        update {
            max_parallel     = 1
            health_check =   "task_states"
            min_healthy_time = "10s"
            healthy_deadline = "1m"
            auto_revert = true
        }
        task "schedueler" {
            driver = "docker"
            config {
                image = "192.168.1.5:5555/workers:${local.version}"
                args = ["celery","-A","workers.tasks","beat","--loglevel=INFO"]
                labels {
                    group = "celery-schedueler"
                }
                logging {
                    type = "fluentd"
                    config {
                        fluentd-address = "192.168.1.5:24224"
                        tag = "schedueler"
                    }
                }
            }
            resources {
                cpu    = 256
                memory = 256
            }
        }
    }
    group "api" {
        count = 2
        scaling {
            enabled = true
            min = 0
            max = 10
            policy {
            }
        }
        update {
            max_parallel     = 1
            canary           = 1
            min_healthy_time = "30s"
            health_check      = "checks"
            healthy_deadline = "5m"
            auto_revert      = true
            auto_promote     = false
        }
        task "api" {
            driver = "docker"
            config {
                image = "192.168.1.5:5555/workers:${local.version}"
                args = ["flask", "run", "--host=0.0.0.0"]
                ports = ["http"]
                labels {
                    group = "celery-api"
                }
                logging {
                    type = "fluentd"
                    config {
                        fluentd-address = "192.168.1.5:24224"
                        tag = "api"
                    }
                }
            }
            resources {
                cpu    = 256
                memory = 256
            }
            env {
                FLASK_APP = "api.py"
            }
        }
        network {
            port "http" {
                to = 5000     
            }
        }
        service {
            name = "queue-api"
            port = "http"
            tags = [
                "traefik.enable=true",
                "traefik.http.routers.http.rule=PathPrefix(`/`)",
            ]
            check {
                type     = "http"
                path     = "/"
                interval = "2s"
                timeout  = "2s"
            }
        }
    }
}