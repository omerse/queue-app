##Start workers
```bash
celery -A workers.tasks worker -c 1 --loglevel=INFO
```

##Submit Scan
```bash
curl -X POST   http://localhost:5000/api/process_scan   -H 'content-type: application/json'   -d '{ "url":"http://s3.amazonaws.com/blob/scan.jpeg" }' -H 'X-Api-Key: asoidewfoef'
```
##Check Scan status
```bash
curl -s localhost:5000/api/status/d62c1b23-43da-4beb-91a5-cd9a4e7b6c49 -H 'X-Api-Key: asoidewfoef'
```