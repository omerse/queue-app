from flask import Flask, json, jsonify, request
from workers.tasks import process_scan, gen_prime
import socket

api = Flask(__name__)

AUTH_TOKEN = "asoidewfoef"


@api.route("/", methods=["GET"])
def index():
    return jsonify(status="ok", host=socket.gethostname())


@api.route("/api/gen_primes", methods=["GET"])
def primes():
    headers = request.headers
    auth = headers.get("X-Api-Key")
    if auth == AUTH_TOKEN:
        url = request.get_json()["url"]
        task = gen_prime.apply_async(args=[2000])
        return jsonify(task_id=task.id)
    else:
        return jsonify({"message": "ERROR: Unauthorized"}), 401


@api.route("/api/process_scan", methods=["POST"])
def process():
    headers = request.headers
    auth = headers.get("X-Api-Key")
    if auth == AUTH_TOKEN:
        url = request.get_json()["url"]
        task = process_scan.apply_async(args=[url])
        return jsonify(task_id=task.id)
    else:
        return jsonify({"message": "ERROR: Unauthorized"}), 401


@api.route("/api/status/<task_id>", methods=["GET"])
def task_status(task_id: str):
    headers = request.headers
    auth = headers.get("X-Api-Key")
    if auth != AUTH_TOKEN:
        return jsonify({"message": "ERROR: Unauthorized"}), 401
    else:
        task = process_scan.AsyncResult(task_id=task_id)
        response = {"id": task_id, "state": task.status}

        return jsonify(response)
