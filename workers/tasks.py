from .celeryconfig import app, logger
from celery.signals import worker_process_init, worker_process_shutdown
import time
from random import randint
import requests
from pymongo import MongoClient

db_conn = None
MONGO_HOST = "192.168.1.5"


@worker_process_init.connect
def init_worker(**kwargs):
    global db_conn
    logger.info("Initializing database connection for worker.")
    db_conn = MongoClient(MONGO_HOST)


@worker_process_shutdown.connect
def shutdown_worker(**kwargs):
    global db_conn
    if db_conn:
        logger.info("Closing database connection for worker.")
        db_conn.close()


@app.task(
    bind=True,
    name="process_scan",
    autoretry_for=(Exception,),
    retry_kwargs={"max_retries": 2, "countdown": 120},
)
def process_scan(self, url):
    time.sleep(randint(5, 8))
    resp = requests.get(url)
    db = db_conn["test-dev"]
    col = db["scans"]
    result = col.insert_one({"url": url})
    return resp.status_code


@app.task(bind=True, name="gen_prime")
def gen_prime(self, x):
    multiples = []
    results = []
    for i in range(2, x + 1):
        if i not in multiples:
            results.append(i)
            for j in range(i * i, x + 1, i):
                multiples.append(j)
    return results


# Schedueled task
@app.task(bind=True, name="add")
def add(self, x, y):
    return x + y
