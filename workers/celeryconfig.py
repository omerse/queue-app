from celery import Celery
from celery.utils.log import get_task_logger
from celery.schedules import crontab
from random import randint


logger = get_task_logger(__name__)

app = Celery(
    "scanapi",
    broker="amqp://admin:password@192.168.1.5:5672/vhost",
    backend="redis://192.168.1.5/0",
)

app.conf.update(
    {
        "task_routes": {
            "process_scan": {"queue": "process_scan"},
            "gen_prime": {"queue": "gen_prime"},
            "add": {"queue": "add"},
        },
        "task_serializer": "json",
        "result_serializer": "json",
        "accept_content": ["json"],
        "timezone": "Asia/Jerusalem",
        "enable_utc": False,
    }
)

###########################################################
# Schedueled tasks
###########################################################
app.conf.beat_schedule = {
    "add-every-ten-min": {
        "task": "add",
        "schedule": crontab(minute="*/10"),
        "args": (randint(1, 100), randint(1, 100)),
        "options": {"queue": "add"},
    },
}
