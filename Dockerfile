FROM python:3.8.10-slim
RUN addgroup --gid 5555 service
RUN adduser --disabled-password --gecos '' --uid 5555 --gid 5555 service

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY workers workers
COPY *.py .

RUN chown service.service ../app -R
USER service